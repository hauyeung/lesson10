using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileInfo
{
    public partial class FileInfo : Form
    {
        public FileInfo()
        {
            InitializeComponent();
        }
 
        private TextBox fileNameTextBox;
        private TextBox pathTextBox;
        private TextBox sizeTextBox;
        private TextBox createdTextBox;
        private TextBox updatedTextBox;
        private CheckBox archiveCheckBox;
        private CheckBox compressedCheckBox;
        private CheckBox hiddenCheckBox;
        private CheckBox normalCheckBox;
        private CheckBox encryptedCheckBox;
        private CheckBox directoryCheckBox;
        private CheckBox readOnlyCheckBox;
        private CheckBox systemCheckBox;
        private Button closeButton;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Button findFileButton;
        private TextBox sizekbTextBox;
        private Label label6;
        private OpenFileDialog openFileDialog = new OpenFileDialog();
	    private void findFileButton_Click(object sender, EventArgs e)
        {
            
            // File dialog settings
            openFileDialog.Multiselect = false;
            openFileDialog.Title = "Select File";
            openFileDialog.Filter = "All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.FileName = "";
            
            // Display file dialog
            DialogResult dialogResult = openFileDialog.ShowDialog();
            
            // Check if user clicked OK button
            if (dialogResult == DialogResult.OK)
            {
                string fullFileName = openFileDialog.FileName;
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(fullFileName);
                fileNameTextBox.Text = fileInfo.Name;
                pathTextBox.Text = fileInfo.DirectoryName;
                sizeTextBox.Text = fileInfo.Length.ToString();
                //sizeLabel.Text = fileInfo.Length.ToString();
                createdTextBox.Text = fileInfo.CreationTime.ToString();
                updatedTextBox.Text = fileInfo.LastWriteTime.ToString();
                archiveCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Archive);
                normalCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Normal);
                readOnlyCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.ReadOnly);
                compressedCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Compressed);
                encryptedCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Encrypted);
                systemCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.System);
                hiddenCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Hidden);
                directoryCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Directory);
                
            }
        }
        
        private bool CheckFileAttribute(System.IO.FileInfo fileInfo, System.IO.FileAttributes fileAttribute)
        {
            return ((fileInfo.Attributes & fileAttribute) == fileAttribute);
        }

        
        private void FileInfo_Load(object sender, EventArgs e)
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(findFileButton, "Select File");
            toolTip.SetToolTip(closeButton, "Close Application");
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileInfo));
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.sizeTextBox = new System.Windows.Forms.TextBox();
            this.createdTextBox = new System.Windows.Forms.TextBox();
            this.updatedTextBox = new System.Windows.Forms.TextBox();
            this.archiveCheckBox = new System.Windows.Forms.CheckBox();
            this.compressedCheckBox = new System.Windows.Forms.CheckBox();
            this.hiddenCheckBox = new System.Windows.Forms.CheckBox();
            this.normalCheckBox = new System.Windows.Forms.CheckBox();
            this.encryptedCheckBox = new System.Windows.Forms.CheckBox();
            this.directoryCheckBox = new System.Windows.Forms.CheckBox();
            this.readOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.systemCheckBox = new System.Windows.Forms.CheckBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.findFileButton = new System.Windows.Forms.Button();
            this.sizekbTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(109, 28);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(457, 20);
            this.fileNameTextBox.TabIndex = 0;
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(112, 67);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(454, 20);
            this.pathTextBox.TabIndex = 1;
            // 
            // sizeTextBox
            // 
            this.sizeTextBox.Location = new System.Drawing.Point(113, 105);
            this.sizeTextBox.Name = "sizeTextBox";
            this.sizeTextBox.Size = new System.Drawing.Size(453, 20);
            this.sizeTextBox.TabIndex = 2;
            // 
            // createdTextBox
            // 
            this.createdTextBox.Location = new System.Drawing.Point(115, 140);
            this.createdTextBox.Name = "createdTextBox";
            this.createdTextBox.Size = new System.Drawing.Size(450, 20);
            this.createdTextBox.TabIndex = 3;
            // 
            // updatedTextBox
            // 
            this.updatedTextBox.Location = new System.Drawing.Point(113, 176);
            this.updatedTextBox.Name = "updatedTextBox";
            this.updatedTextBox.Size = new System.Drawing.Size(451, 20);
            this.updatedTextBox.TabIndex = 4;
            // 
            // archiveCheckBox
            // 
            this.archiveCheckBox.AutoSize = true;
            this.archiveCheckBox.Location = new System.Drawing.Point(78, 258);
            this.archiveCheckBox.Name = "archiveCheckBox";
            this.archiveCheckBox.Size = new System.Drawing.Size(62, 17);
            this.archiveCheckBox.TabIndex = 5;
            this.archiveCheckBox.Text = "Archive";
            this.archiveCheckBox.UseVisualStyleBackColor = true;
            // 
            // compressedCheckBox
            // 
            this.compressedCheckBox.AutoSize = true;
            this.compressedCheckBox.Location = new System.Drawing.Point(196, 261);
            this.compressedCheckBox.Name = "compressedCheckBox";
            this.compressedCheckBox.Size = new System.Drawing.Size(84, 17);
            this.compressedCheckBox.TabIndex = 6;
            this.compressedCheckBox.Text = "Compressed";
            this.compressedCheckBox.UseVisualStyleBackColor = true;
            // 
            // hiddenCheckBox
            // 
            this.hiddenCheckBox.AutoSize = true;
            this.hiddenCheckBox.Location = new System.Drawing.Point(336, 258);
            this.hiddenCheckBox.Name = "hiddenCheckBox";
            this.hiddenCheckBox.Size = new System.Drawing.Size(60, 17);
            this.hiddenCheckBox.TabIndex = 7;
            this.hiddenCheckBox.Text = "Hidden";
            this.hiddenCheckBox.UseVisualStyleBackColor = true;
            // 
            // normalCheckBox
            // 
            this.normalCheckBox.AutoSize = true;
            this.normalCheckBox.Location = new System.Drawing.Point(77, 304);
            this.normalCheckBox.Name = "normalCheckBox";
            this.normalCheckBox.Size = new System.Drawing.Size(59, 17);
            this.normalCheckBox.TabIndex = 8;
            this.normalCheckBox.Text = "Normal";
            this.normalCheckBox.UseVisualStyleBackColor = true;
            // 
            // encryptedCheckBox
            // 
            this.encryptedCheckBox.AutoSize = true;
            this.encryptedCheckBox.Location = new System.Drawing.Point(198, 304);
            this.encryptedCheckBox.Name = "encryptedCheckBox";
            this.encryptedCheckBox.Size = new System.Drawing.Size(74, 17);
            this.encryptedCheckBox.TabIndex = 9;
            this.encryptedCheckBox.Text = "Encrypted";
            this.encryptedCheckBox.UseVisualStyleBackColor = true;
            // 
            // directoryCheckBox
            // 
            this.directoryCheckBox.AutoSize = true;
            this.directoryCheckBox.Location = new System.Drawing.Point(337, 306);
            this.directoryCheckBox.Name = "directoryCheckBox";
            this.directoryCheckBox.Size = new System.Drawing.Size(68, 17);
            this.directoryCheckBox.TabIndex = 10;
            this.directoryCheckBox.Text = "Directory";
            this.directoryCheckBox.UseVisualStyleBackColor = true;
            // 
            // readOnlyCheckBox
            // 
            this.readOnlyCheckBox.AutoSize = true;
            this.readOnlyCheckBox.Location = new System.Drawing.Point(77, 348);
            this.readOnlyCheckBox.Name = "readOnlyCheckBox";
            this.readOnlyCheckBox.Size = new System.Drawing.Size(76, 17);
            this.readOnlyCheckBox.TabIndex = 11;
            this.readOnlyCheckBox.Text = "Read Only";
            this.readOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // systemCheckBox
            // 
            this.systemCheckBox.AutoSize = true;
            this.systemCheckBox.Location = new System.Drawing.Point(198, 347);
            this.systemCheckBox.Name = "systemCheckBox";
            this.systemCheckBox.Size = new System.Drawing.Size(60, 17);
            this.systemCheckBox.TabIndex = 12;
            this.systemCheckBox.Text = "System";
            this.systemCheckBox.UseVisualStyleBackColor = true;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(490, 414);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(111, 39);
            this.closeButton.TabIndex = 14;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "File Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Path";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Size";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Created";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Updated";
            // 
            // findFileButton
            // 
            this.findFileButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("findFileButton.BackgroundImage")));
            this.findFileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.findFileButton.Location = new System.Drawing.Point(609, 31);
            this.findFileButton.Name = "findFileButton";
            this.findFileButton.Size = new System.Drawing.Size(95, 35);
            this.findFileButton.TabIndex = 20;
            this.findFileButton.UseVisualStyleBackColor = true;
            this.findFileButton.Click += new System.EventHandler(this.findFileButton_Click_1);
            // 
            // sizekbTextBox
            // 
            this.sizekbTextBox.Location = new System.Drawing.Point(116, 213);
            this.sizekbTextBox.Name = "sizekbTextBox";
            this.sizekbTextBox.Size = new System.Drawing.Size(450, 20);
            this.sizekbTextBox.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Size (kB)";
            // 
            // FileInfo
            // 
            this.ClientSize = new System.Drawing.Size(749, 487);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sizekbTextBox);
            this.Controls.Add(this.findFileButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.systemCheckBox);
            this.Controls.Add(this.readOnlyCheckBox);
            this.Controls.Add(this.directoryCheckBox);
            this.Controls.Add(this.encryptedCheckBox);
            this.Controls.Add(this.normalCheckBox);
            this.Controls.Add(this.hiddenCheckBox);
            this.Controls.Add(this.compressedCheckBox);
            this.Controls.Add(this.archiveCheckBox);
            this.Controls.Add(this.updatedTextBox);
            this.Controls.Add(this.createdTextBox);
            this.Controls.Add(this.sizeTextBox);
            this.Controls.Add(this.pathTextBox);
            this.Controls.Add(this.fileNameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FileInfo";
            this.Text = "Search";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void findFileButton_Click_1(object sender, EventArgs e)
        {
            // File dialog settings
            openFileDialog.Multiselect = false;
            openFileDialog.Title = "Select File";
            openFileDialog.Filter = "All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.FileName = "";

            // Display file dialog
            DialogResult dialogResult = openFileDialog.ShowDialog();

            // Check if user clicked OK button
            if (dialogResult == DialogResult.OK)
            {
                string fullFileName = openFileDialog.FileName;
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(fullFileName);
                fileNameTextBox.Text = fileInfo.Name;
                pathTextBox.Text = fileInfo.DirectoryName;
                sizeTextBox.Text = fileInfo.Length.ToString();
                sizekbTextBox.Text = (fileInfo.Length / 1024).ToString();
                createdTextBox.Text = fileInfo.CreationTime.ToString();
                updatedTextBox.Text = fileInfo.LastWriteTime.ToString();
                archiveCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Archive);
                normalCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Normal);
                readOnlyCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.ReadOnly);
                compressedCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Compressed);
                encryptedCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Encrypted);
                systemCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.System);
                hiddenCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Hidden);
                directoryCheckBox.Checked = CheckFileAttribute(fileInfo, System.IO.FileAttributes.Directory);
            }
        }

        private void fileSizeLabel_Click(object sender, EventArgs e)
        {

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
